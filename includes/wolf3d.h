/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 19:49:48 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 15:52:24 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOLF3D_H
# define WOLF3D_H

# include "libft.h"
# include "mlx.h"
# include "key_hooks.h"

# define NORTH 0
# define EAST 2
# define SOUTH 1
# define WEST 3

# define MOVE 0.15f
# define ROTATE 0.075f

# define WIDTH 600
# define HEIGHT 600

# define GEN_TEXTURE_WIDTH 96
# define GEN_TEXTURE_HEIGHT 96
# define GEN_TEXTURE_NUMBER 8

# define TEXTURE_NUM 16

typedef float	t_vector __attribute__((vector_size(sizeof(float)*2)));

typedef int		t_pair __attribute__((vector_size(sizeof(int)*2)));

typedef int		t_rgb __attribute__((vector_size(sizeof(int)*3)));

typedef struct	s_gamemap
{
	t_vector		player;
	int				**map;
	int				map_width;
	int				map_height;
	int				map_size;
	int				screen_width;
	int				screen_height;
}				t_gamemap;

typedef struct	s_img
{
	void			*img_ptr;
	int				*img_arr;
	int				pixel_mass;
	int				line_mass;
	int				width;
	int				height;
	int				img_mass;
	int				endi;
}				t_img;

typedef struct	s_line
{
	t_vector		raydir;
	t_vector		deltadist;
	t_pair			pos;
	double			wallx;
	double			perpwalldist;
	double			lineheight;
	int				x;
}				t_line;

typedef struct	s_info
{
	t_line			line;
	t_vector		player;
	t_vector		rotation;
	t_vector		plane;
	t_gamemap		*gamemap;
	t_img			**ld_textures;
	t_img			**gen_textures;
	t_img			*floor;
	t_img			*ceiling;
	t_img			*image;
	void			*mlxptr;
	void			*winptr;
}				t_info;

/*
** MLX
*/

int				mlx_starts(int (*trigger_func)(), t_info *inf);
int				create_image(int map_x, int map_y, t_img *image, void *mlxptr);

int				triggers(int key, void *p);

/*
** Camera
*/

t_vector		view2d(t_info *inf, int x, t_vector planexy);

t_vector		rotate_vector(t_vector vector, double rotate);

/*
** Color
*/

void			drawstyle_basic(t_line *line, t_pair start_end,
					int side, t_info *inf);
void			drawstyle_ld_texture(t_line *line, t_pair start_end,
					int side, t_info *inf);
void			drawstyle_gen_texture(t_line *line, t_pair start_end,
					int side, t_info *inf);
void			drawstyle_door(t_line *line, t_pair start_end,
					int side, t_info *inf);

/*
** Parsing
*/

t_gamemap		*parse_map(char *filename);
int				**alloc_gamemap(int x, int y, int size);

void			read_info(char **arr, t_gamemap *gamemap);
int				len2arr(char **arr);
int				check2arr(char **arr, int line);
int				is_full_wall(char **str);
void			readmap_init(char ****map, char **str, int lines);

/*
** Setters
*/

int				putpixel(t_img *image, t_rgb *color, int x, int y);
int				putpixel_int(t_img *image, int color, int x, int y);

/*
** Render
*/

void			main_loop(t_info *inf);
void			render_col(t_line *line, t_info *inf);

void			draw_floor_ceiling(t_info *inf, t_pair start_end,
									t_line *line, int side);

/*
** Textures
*/

t_img			**load_textures(void *mlxptr);
t_img			*load_texture(void *mlxptr, char *filename);

t_img			**gen_textures(void *mlxptr);

int				get_texturex(t_line *line, int side, t_img *texture);

/*
** Errors
*/

void			errmsg(const char *str);
void			perrmsg(const char *str);

#endif
