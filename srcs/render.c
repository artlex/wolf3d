/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/18 17:49:27 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 15:03:50 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

void	choose_drawstyle(t_line *line, t_pair start_end, int side, t_info *inf)
{
	if (inf->gamemap->map[line->pos[1]][line->pos[0]] < 10)
		drawstyle_basic(line, start_end, side, inf);
	else if (inf->gamemap->map[line->pos[1]][line->pos[0]] < 20)
		drawstyle_ld_texture(line, start_end, side, inf);
	else if (inf->gamemap->map[line->pos[1]][line->pos[0]] < 30)
		drawstyle_gen_texture(line, start_end, side, inf);
	else if (inf->gamemap->map[line->pos[1]][line->pos[0]] == 90)
		drawstyle_door(line, start_end, side, inf);
}

void	find_line(t_info *inf, t_line *line, int side)
{
	int		lineheight;
	t_pair	start_end;

	lineheight = (int)(inf->gamemap->screen_height / line->perpwalldist);
	start_end[0] = -lineheight / 2 + inf->gamemap->screen_height / 2;
	start_end[0] = start_end[0] < 0 ? 0 : start_end[0];
	start_end[1] = lineheight / 2 + inf->gamemap->screen_height / 2;
	start_end[1] = start_end[1] >= inf->gamemap->screen_height ?
		inf->gamemap->screen_height - 1 : start_end[1];
	line->lineheight = lineheight;
	line->wallx = (side == EAST || side == WEST) ?
		inf->player[1] + line->perpwalldist * line->raydir[1] :
		inf->player[0] + line->perpwalldist * line->raydir[0];
	line->wallx -= floor(line->wallx);
	choose_drawstyle(line, start_end, side, inf);
	draw_floor_ceiling(inf, start_end, line, side);
}

int		get_wall(t_info *inf, t_line *line, t_vector sidedist, t_pair step)
{
	int		hit;
	int		side;

	hit = 0;
	while (hit == 0)
	{
		if (sidedist[0] < sidedist[1])
		{
			sidedist[0] += line->deltadist[0];
			line->pos[0] += step[0];
			side = line->pos[0] > inf->player[0] ? WEST : EAST;
		}
		else
		{
			sidedist[1] += line->deltadist[1];
			line->pos[1] += step[1];
			side = line->pos[1] > inf->player[1] ? SOUTH : NORTH;
		}
		if (inf->gamemap->map[line->pos[1]][line->pos[0]] > 0)
			hit = 1;
	}
	return (side);
}

void	render(t_info *inf, t_line *line)
{
	t_vector	sidedist;
	t_pair		step;
	int			side;

	step = (t_pair){(line->raydir[0] < 0 ? -1 : 1),
		(line->raydir[1] < 0 ? -1 : 1)};
	if (line->raydir[0] < 0)
		sidedist[0] = (inf->player[0] - line->pos[0]) * line->deltadist[0];
	else
		sidedist[0] = (line->pos[0] + 1 - inf->player[0]) * line->deltadist[0];
	if (line->raydir[1] < 0)
		sidedist[1] = (inf->player[1] - line->pos[1]) * line->deltadist[1];
	else
		sidedist[1] = (line->pos[1] + 1 - inf->player[1]) * line->deltadist[1];
	side = get_wall(inf, line, sidedist, step);
	if (side == EAST || side == WEST)
		line->perpwalldist = (line->pos[0] - inf->player[0] +
				(1 - step[0]) / 2) / line->raydir[0];
	else
		line->perpwalldist = (line->pos[1] - inf->player[1] +
				(1 - step[1]) / 2) / line->raydir[1];
	find_line(inf, line, side);
}

void	render_col(t_line *line, t_info *inf)
{
	line->raydir = view2d(inf, line->x, inf->plane);
	line->deltadist = (t_vector){fabs(1 / line->raydir[0]),
		fabs(1 / line->raydir[1])};
	line->pos[0] = (int)inf->player[0];
	line->pos[1] = (int)inf->player[1];
	render(inf, line);
}
