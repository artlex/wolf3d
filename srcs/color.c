/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   color.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 16:35:38 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 14:38:49 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

void	drawstyle_basic(t_line *line, t_pair start_end, int side, t_info *inf)
{
	int				y;
	t_rgb			newcolor;
	const t_rgb		g_basic[][4] = {{{255, 0, 0}, {0, 255, 0},
	{0, 0, 255}, {100, 100, 100}}};

	y = start_end[0];
	newcolor = g_basic[inf->gamemap->map[line->pos[1]][line->pos[0]] - 1][side];
	while (y < start_end[1])
	{
		if (putpixel(inf->image, &newcolor, y, line->x) == -1)
			errmsg("Cannot put pixel to image for some reason");
		++y;
	}
}

void	drawstyle_ld_texture(t_line *line, t_pair start_end,
					int side, t_info *inf)
{
	const int	map_val = inf->gamemap->map[line->pos[1]][line->pos[0]];
	t_pair		tex_xy;
	t_img		*texture;
	int			y;
	int			help;

	y = start_end[0];
	texture = inf->ld_textures[(map_val - 10) * 4 + side];
	tex_xy[0] = get_texturex(line, side, texture);
	while (y < start_end[1])
	{
		help = y * 256 - inf->gamemap->screen_height * 128 +
			line->lineheight * 128;
		tex_xy[1] = ((help * texture->height) / line->lineheight) / 256;
		if (putpixel_int(inf->image, texture->img_arr[tex_xy[1] *
					texture->width + tex_xy[0]], y, line->x) == -1)
			errmsg("Cannot put pixel to image for some reason");
		++y;
	}
}

void	drawstyle_gen_texture(t_line *line, t_pair start_end,
					int side, t_info *inf)
{
	const int	map_val = inf->gamemap->map[line->pos[1]][line->pos[0]];
	t_pair		tex_xy;
	t_img		*texture;
	int			y;
	int			help;

	y = start_end[0];
	texture = inf->gen_textures[(map_val - 20) * 4 + side];
	tex_xy[0] = get_texturex(line, side, texture);
	while (y < start_end[1])
	{
		help = y * 256 - inf->gamemap->screen_height * 128 +
			line->lineheight * 128;
		tex_xy[1] = ((help * texture->height) / line->lineheight) / 256;
		if (putpixel_int(inf->image, texture->img_arr[tex_xy[1] *
					texture->width + tex_xy[0]], y, line->x) == -1)
			errmsg("Cannot put pixel to image for some reason");
		++y;
	}
}

void	drawstyle_door(t_line *line, t_pair start_end, int side, t_info *inf)
{
	const int	mask = 0xFFFFFF;
	t_pair		tex_xy;
	t_img		*texture;
	int			y;
	int			help;

	y = start_end[0];
	texture = inf->ld_textures[4];
	tex_xy[0] = get_texturex(line, side, texture);
	while (y < start_end[1])
	{
		help = y * 256 - inf->gamemap->screen_height * 128 +
			line->lineheight * 128;
		tex_xy[1] = ((help * texture->height) / line->lineheight) / 256;
		if (putpixel_int(inf->image,
		(texture->img_arr[tex_xy[1] * texture->width + tex_xy[0]] << 1) & mask,
			y, line->x) == -1)
			errmsg("Cannot put pixel to image for some reason");
		++y;
	}
}
