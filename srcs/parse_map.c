/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 17:19:20 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 15:52:25 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <fcntl.h>

int			open_map(char *filename)
{
	char	*arr;
	char	a;
	int		fd;

	if (!(arr = ft_strstr(filename, ".w3m")) || ft_strlen(arr) != 4)
		errmsg("Map name must have format like \"map.w3m\" \
with no repeating \".w3m\"");
	if ((fd = open(filename, O_RDONLY)) == -1)
		perrmsg("Cannot open file");
	if (read(fd, &a, 0) == -1)
		perrmsg("Cannot read from file");
	return (fd);
}

char		***read_map(int fd, int lines, int cols)
{
	char	*str;
	char	**arr;
	int		i;
	int		ret;
	char	***map;

	i = -1;
	readmap_init(&map, &str, lines);
	while (++i < lines)
	{
		if ((ret = get_next_line(fd, &str)) == -1)
			perrmsg("Error occured while reading map");
		if (ret == 0)
			errmsg("Wrong number of lines");
		if (!(arr = ft_strsplit(str, ' ')))
			perrmsg("Error on splitting map");
		ft_strdel(&str);
		if (!check2arr(arr, i))
			errmsg("Map must contain only permitted values. Read usage");
		if (ft_matrixlen(arr) != cols)
			errmsg("All map lines must have fixed amount of numbers");
		map[i] = arr;
	}
	return (map);
}

void		get_info(t_gamemap *gamemap, int fd)
{
	char	*str;
	char	**arr;
	int		ret;

	str = NULL;
	if ((ret = get_next_line(fd, &str)) == -1)
		perrmsg("Error occured while reading map");
	if (ret == 0)
		errmsg("Empty file");
	if (!(arr = ft_strsplit(str, ' ')))
		perrmsg("Cannot allocate array for map info");
	if (ft_matrixlen(arr) != 4)
		errmsg("First line of map must have information about map \
size and player coordinates, only 4! positive numbers");
	read_info(arr, gamemap);
	gamemap->map_size = gamemap->map_width * gamemap->map_height;
	if (gamemap->map_width < 3 || gamemap->map_height < 3 ||
			gamemap->map_width > 40 || gamemap->map_height > 40)
		errmsg("Map width and height cannot be lesser \
than 3 or bigger than 40");
	free(str);
	ft_freematrix(arr);
}

void		fill_gamemap(t_gamemap *gamemap, char ***arr)
{
	int		i;
	int		j;

	i = 0;
	gamemap->map = alloc_gamemap(gamemap->map_width,
			gamemap->map_height, gamemap->map_size);
	while (i < gamemap->map_height)
	{
		j = 0;
		while (j < gamemap->map_width)
		{
			gamemap->map[i][j] = ft_atoi(arr[i][j]);
			free(arr[i][j]);
			++j;
		}
		free(arr[i]);
		++i;
	}
	free(arr);
}

t_gamemap	*parse_map(char *filename)
{
	t_gamemap	*gamemap;
	int			fd;
	char		***arr;
	char		buff[512];

	buff[0] = 0;
	if (!(gamemap = (t_gamemap *)malloc(sizeof(t_gamemap))))
		perrmsg("Cannot create gamemap struct");
	fd = open_map(filename);
	get_info(gamemap, fd);
	if (gamemap->player[0] >= gamemap->map_width || gamemap->player[0] <= 0 ||
		gamemap->player[1] >= gamemap->map_height || gamemap->player[1] <= 0)
		errmsg("You cat put player only between map borders (more than 0 \
and smaller than width/height - 1)");
	arr = read_map(fd, gamemap->map_height, gamemap->map_width);
	fill_gamemap(gamemap, arr);
	return (gamemap);
}
