/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 16:40:49 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

static void	usage(int mode)
{
	if (mode == 0)
	{
		ft_printf("usage:  {green}./wolf3d \"filename.w3m\" to play\n\
\t{yellow}./wolf3d --help to see more info about wolf3d\n");
		exit(EXIT_SUCCESS);
	}
	ft_printf("Keybinds:\n  Arrows up and down to move\n  \
Arrows left and right to rotate camera\n  \'E\' button to open doors\n\n\
How to create map:\n  First line of map must contain 4 positive numbers: \
width and height of map, x and y coordinates of player.\n  You cannot create \
map smaller than 3x3 or bigger than 40x40 and can put player only on floor \
inside map borders.\n  Next lines of map must contain fixed amount of \
numbers.  Thesee numbers can have only allowed values.\n  \
Each number is responsible for unique texture.\n  \
There are information about allowed numbers:\n   \
0 - floor\n   \
1 - red, green, blue and grey colors\n   \
10, 11, 12, 13 - sets of downloaded textures\n   \
20, 21 - sets of generated textures\n   \
90 - door\n");
	exit(EXIT_SUCCESS);
}

static void	check_borders(t_gamemap *gamemap)
{
	int				i;
	int				j;
	const t_pair	pl = {(int)gamemap->player[0], (int)gamemap->player[1]};
	const int		width = gamemap->map_width;
	const int		height = gamemap->map_height;

	i = -1;
	j = -1;
	if (gamemap->map[pl[1]][pl[0]])
		errmsg("Player cannot be put on wall");
	while (++i < width)
		if (!gamemap->map[0][i] || !gamemap->map[height - 1][i] ||
				gamemap->map[0][i] == 90 || gamemap->map[height - 1][i] == 90)
			errmsg("Map borders must be walls");
	while (++j < height)
		if (!gamemap->map[j][0] || !gamemap->map[j][width - 1] ||
				gamemap->map[j][0] == 90 || gamemap->map[j][width - 1] == 90)
			errmsg("Map borders must be walls");
}

static void	init_camera(t_info *inf, t_gamemap *gamemap)
{
	gamemap->player[0] += 0.5;
	gamemap->player[1] += 0.5;
	inf->player = (t_vector){gamemap->player[0], gamemap->player[1]};
	inf->rotation = (t_vector){-1, 0};
	inf->plane = (t_vector){0, 0.66};
	gamemap->screen_width = WIDTH;
	gamemap->screen_height = HEIGHT;
}

void		main_loop(t_info *inf)
{
	t_line			line;

	ft_bzero(inf->image->img_arr, inf->image->img_mass * sizeof(int));
	line.x = 0;
	while (line.x < inf->gamemap->screen_width)
	{
		render_col(&line, inf);
		++line.x;
	}
	mlx_put_image_to_window(inf->mlxptr, inf->winptr,
			inf->image->img_ptr, 0, 0);
}

int			main(int ac, char **av)
{
	t_info	inf;

	if (ac != 2)
		usage(0);
	else if (ac == 2 && ft_strequ(av[1], "--help"))
		usage(1);
	inf.gamemap = parse_map(av[1]);
	check_borders(inf.gamemap);
	init_camera(&inf, inf.gamemap);
	mlx_starts(triggers, &inf);
	inf.ld_textures = load_textures(inf.mlxptr);
	inf.gen_textures = gen_textures(inf.mlxptr);
	inf.ceiling = inf.ld_textures[0];
	inf.floor = inf.ld_textures[4];
	main_loop(&inf);
	mlx_loop(inf.mlxptr);
}
