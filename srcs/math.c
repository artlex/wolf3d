/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 19:44:27 by bcherkas          #+#    #+#             */
/*   Updated: 2018/12/13 16:11:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

float	get_radian(float angle)
{
	return (M_PI * angle / 180);
}

int		sq_equation(t_vector k, float *res)
{
	float	t[2];
	float	discriminant;

	discriminant = pow(k[1], 2) - 4 * k[0] * k[2];
	if (discriminant < 0)
		return (0);
	else if (discriminant > 0)
		discriminant = sqrt(discriminant);
	t[0] = (-k[1] - discriminant) / k[0] / 2;
	t[1] = (-k[1] + discriminant) / k[0] / 2;
	*res = t[0] < t[1] ? t[0] : t[1];
	return (1);
}
