/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   floor_ceiling.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 16:47:27 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/07 17:35:53 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

/*
** wpc: w - distwall, p p  - distplayer, c - currentdist
** floors: floors[0] - floortexture, floors[1] - floor current
*/

static void		loop(t_vector floor, t_info *inf, int y, t_line *line)
{
	const int	h = inf->gamemap->screen_height;
	t_vector	floors[2];
	double		weight;
	int			el;
	double		wpc[3];

	wpc[0] = line->perpwalldist;
	wpc[1] = 0;
	while (++y < h)
	{
		wpc[2] = h / (2.0 * y - h);
		weight = (wpc[2] - wpc[1]) / (wpc[0] - wpc[1]);
		floors[1][0] = weight * floor[0] + (1.0 - weight) * inf->player[0];
		floors[1][1] = weight * floor[1] + (1.0 - weight) * inf->player[1];
		floors[0][0] = (int)(floors[1][0] * inf->ceiling->width) %
			inf->ceiling->width;
		floors[0][1] = (int)(floors[1][1] * inf->ceiling->height) %
			inf->ceiling->height;
		el = inf->ceiling->width * floors[0][1] + floors[0][0];
		putpixel_int(inf->image, inf->ceiling->img_arr[el], y, line->x);
		putpixel_int(inf->image, inf->floor->img_arr[el], h - y, line->x);
	}
}

void			draw_floor_ceiling(t_info *inf, t_pair start_end,
								t_line *line, int sd)
{
	const int	h = inf->gamemap->screen_height;
	t_vector	floor;

	if ((sd == EAST || sd == WEST) && line->raydir[0] > 0)
		floor = (t_vector){line->pos[0], line->pos[1] + line->wallx};
	else if ((sd == EAST || sd == WEST) && line->raydir[0] < 0)
		floor = (t_vector){line->pos[0] + 1.0, line->pos[1] + line->wallx};
	else if ((sd == SOUTH || sd == NORTH) && line->raydir[1] > 0)
		floor = (t_vector){line->pos[0] + line->wallx, line->pos[1]};
	else
		floor = (t_vector){line->pos[0] + line->wallx, line->pos[1] + 1.0};
	if (start_end[1] < 0)
		start_end[1] = h;
	loop(floor, inf, start_end[1], line);
}
