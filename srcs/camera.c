/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   camera.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 14:59:31 by bcherkas          #+#    #+#             */
/*   Updated: 2018/12/20 18:48:05 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

t_vector		view2d(t_info *inf, int x, t_vector planexy)
{
	const double	camx = 2 * x / (double)inf->gamemap->screen_width - 1;
	t_vector		vector;

	vector[0] = inf->rotation[0] + planexy[0] * camx;
	vector[1] = inf->rotation[1] + planexy[1] * camx;
	return (vector);
}

t_vector		rotate_vector(t_vector vector, double rot)
{
	const double	ssin = sin(rot);
	const double	scos = cos(rot);
	t_vector		res;

	res[0] = vector[0] * scos - vector[1] * ssin;
	res[1] = vector[0] * ssin + vector[1] * scos;
	return (res);
}
