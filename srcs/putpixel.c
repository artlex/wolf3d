/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getters.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/20 13:21:47 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 14:44:10 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

int				rgb_to_int(t_rgb c)
{
	return ((c[0] << 16) | (c[1] << 8) | c[2]);
}

int				putpixel(t_img *img, t_rgb *color, int y, int x)
{
	int				el;

	if (!color || (*color)[0] < 0 || (*color)[1] < 0 || (*color)[2] < 0 ||
			(*color)[0] > 255 || (*color)[1] > 255 || (*color)[2] > 255)
		return (-1);
	else if (x < 0 || y < 0)
		return (-1);
	else if ((el = y * img->width + x) >= img->img_mass)
		return (-1);
	img->img_arr[el] = rgb_to_int(*color);
	return (0);
}

int				putpixel_int(t_img *img, int color, int y, int x)
{
	int				el;

	if (x < 0 || y < 0)
		return (-1);
	else if ((el = y * img->width + x) >= img->img_mass)
		return (-1);
	img->img_arr[el] = color;
	return (0);
}
