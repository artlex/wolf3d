/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 17:24:41 by bcherkas          #+#    #+#             */
/*   Updated: 2018/12/20 15:17:33 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <stdio.h>

void		perrmsg(const char *str)
{
	perror(str);
	exit(1);
}

void		errmsg(const char *str)
{
	ft_dprintf(2, "error: {red}%s\n", str);
	exit(1);
}
