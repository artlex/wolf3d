/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   textures.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 16:47:40 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 14:41:07 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

t_img			**load_textures(void *mlxptr)
{
	int const	texturenum = TEXTURE_NUM;
	t_img		**texturearr;
	int			i;
	const char	*(texturefiles[texturenum + 1]) = {"textures/bluestone.xpm",
	"textures/colorstone.xpm", "textures/eagle.xpm",
	"textures/greystone.xpm", "textures/wood.xpm", "textures/mossy.xpm",
	"textures/purplestone.xpm", "textures/redbrick.xpm",
	"textures/sunshine.xpm", "textures/leopold.xpm", "textures/astadnik1.xpm",
	"textures/astadnik2.xpm", "textures/kage1.xpm", "textures/kage2.xpm",
	"textures/kage3.xpm", "textures/kage4.xpm", NULL};

	i = 0;
	texturearr = (t_img **)malloc(sizeof(t_img *) * (texturenum + 1));
	if (!texturearr)
		perrmsg("texturearr: ");
	texturearr[texturenum] = NULL;
	while (i < texturenum)
	{
		texturearr[i] = load_texture(mlxptr, ft_strdup(texturefiles[i]));
		++i;
	}
	return (texturearr);
}

t_img			*load_texture(void *mlxptr, char *filename)
{
	t_img		*image;

	image = (t_img *)malloc(sizeof(t_img));
	if (!image)
		perrmsg("Cannot allocate memory for texture struct");
	image->img_ptr = mlx_xpm_file_to_image(mlxptr, filename,
								&image->width, &image->height);
	if (!image->img_ptr)
	{
		ft_dprintf(2, "error: {red}Cannot create texture from file %s\n",
				filename);
		exit(EXIT_FAILURE);
	}
	image->pixel_mass = 8;
	image->line_mass = image->pixel_mass * image->width;
	image->endi = 0;
	image->img_mass = image->width * image->height;
	image->img_arr = (int *)mlx_get_data_addr(image->img_ptr,
		&(image->pixel_mass), &(image->line_mass), &(image->endi));
	if (!image->img_arr)
		errmsg("Cannot create texture array");
	free(filename);
	return (image);
}

int				get_texturex(t_line *line, int side, t_img *texture)
{
	int		texx;

	texx = (int)(line->wallx * (double)texture->width);
	if ((side == EAST || side == WEST) && line->raydir[0] > 0)
		texx = texture->width - texx - 1;
	if ((side == SOUTH || side == NORTH) && line->raydir[1] < 0)
		texx = texture->width - texx - 1;
	return (texx);
}

/*
** xorcolor - color[0], xycolor - color[1], xcolor - color[2], ycolor - color[3]
*/

static void		generate(int **tx)
{
	const int	hw[2] = {GEN_TEXTURE_WIDTH, GEN_TEXTURE_HEIGHT};
	int			y;
	int			x;
	int			col[4];

	x = -1;
	while (++x < hw[0])
	{
		y = -1;
		while (++y < hw[1])
		{
			col[0] = (x * 256 / hw[0]) ^ (y * 256 / hw[1]);
			col[1] = y * 128 / hw[1] + x * 128 / hw[0];
			col[2] = x * 256 / hw[0];
			col[3] = y * 256 / hw[1];
			tx[0][hw[0] * y + x] = 65536 * 254 * (x != y && x != hw[0] - y);
			tx[1][hw[0] * y + x] = col[1] + 256 * col[1] + 65536 * col[1];
			tx[2][hw[0] * y + x] = 256 * col[1] + 65536 * col[1];
			tx[3][hw[0] * y + x] = col[0] + 256 * col[0] + 65536 * col[0];
			tx[4][hw[0] * y + x] = 256 * col[0];
			tx[5][hw[0] * y + x] = 65536 * 192 * (x % 16 && y % 16);
			tx[6][hw[0] * y + x] = 65536 * col[3];
			tx[7][hw[0] * y + x] = 128 + 256 * 128 + 65536 * 128;
		}
	}
}

t_img			**gen_textures(void *mlxptr)
{
	const int	numgen = GEN_TEXTURE_NUMBER;
	t_img		**texturearr;
	int			i;
	int			*(array[numgen]);

	i = -1;
	if (!(texturearr = (t_img **)malloc(sizeof(t_img *) * (numgen + 1))))
		perrmsg("generate texturearr: ");
	texturearr[numgen] = NULL;
	while (++i < numgen)
	{
		if (!(texturearr[i] = (t_img *)malloc(sizeof(t_img))))
			perrmsg("generated texturearr");
		if (create_image(GEN_TEXTURE_WIDTH, GEN_TEXTURE_HEIGHT,
					texturearr[i], mlxptr))
			errmsg("Cannot create image for generated texture");
		array[i] = texturearr[i]->img_arr;
	}
	generate(array);
	return (texturearr);
}
