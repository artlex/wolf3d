/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_trigger.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/08 16:40:58 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"
#include <math.h>

int		camera_move(int key, t_info *inf)
{
	t_vector	pos;
	t_vector	change;

	change = (key == ARROW_UP) ? inf->rotation * (t_vector){MOVE, MOVE} :
		inf->rotation * (t_vector){-MOVE, -MOVE};
	pos = inf->player + change;
	if (inf->gamemap->map[(int)(pos[1] +
				change[1])][(int)(pos[0] + change[0])] == 0)
		inf->player = pos;
	main_loop(inf);
	return (1);
}

int		camera_rotate(int key, t_info *inf)
{
	inf->rotation = rotate_vector(inf->rotation,
			(key == ARROW_RIGHT) ? -ROTATE : ROTATE);
	inf->plane = rotate_vector(inf->plane,
			(key == ARROW_RIGHT) ? -ROTATE : ROTATE);
	main_loop(inf);
	return (0);
}

int		open_door(t_info *inf)
{
	t_line	line;

	line.x = inf->gamemap->screen_width / 2;
	render_col(&line, inf);
	if (inf->gamemap->map[(int)line.pos[1]][(int)line.pos[0]] == 90 &&
			inf->player[0] + inf->rotation[0] >= (int)line.pos[0] &&
			inf->player[1] + inf->rotation[1] >= (int)line.pos[1])
		inf->gamemap->map[(int)line.pos[1]][(int)line.pos[0]] = 0;
	else
		return (1);
	main_loop(inf);
	return (0);
}

int		triggers(int key, void *p)
{
	t_info *const	inf = (t_info *)p;

	if (key == ARROW_UP || key == ARROW_DOWN)
		camera_move(key, inf);
	else if (key == ARROW_LEFT || key == ARROW_RIGHT)
		camera_rotate(key, inf);
	else if (key == KEY_E)
		open_door(inf);
	return (0);
}
