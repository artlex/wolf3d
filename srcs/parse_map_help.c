/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_map_help.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/13 17:55:10 by bcherkas          #+#    #+#             */
/*   Updated: 2019/01/12 14:45:11 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "wolf3d.h"

void		readmap_init(char ****map, char **str, int lines)
{
	if (str)
		*str = NULL;
	if (map)
	{
		*map = (char ***)malloc(sizeof(char **) * (lines + 1));
		if (*map == NULL)
			perrmsg("Error while creating parse-helping map array");
		(*map)[lines] = NULL;
	}
}

int			has_right_val(char *str, int line, int col)
{
	int const	numrightvals = 9;
	int			i;
	int			ret;
	char const	*(rightvals[numrightvals]) = {"0", "1", "10", "11", "12",
	"13", "20", "21", "90"};

	i = 0;
	ret = 0;
	while (i < numrightvals)
	{
		ret += ft_strequ(str, rightvals[i]);
		++i;
	}
	if (ret != 1)
	{
		ft_dprintf(2, "Not permitted value on line %d and col %d of map: \
{red}%s\n", line + 1, col + 1, str);
		return (0);
	}
	return (ret);
}

int			check2arr(char **arr, int line)
{
	int		i;

	i = 0;
	while (arr[i])
	{
		if (!has_right_val(arr[i], line, i))
			return (0);
		++i;
	}
	return (1);
}

int			**alloc_gamemap(int x, int y, int size)
{
	int		**map;
	int		i;

	i = 1;
	if (!(map = (int **)malloc(sizeof(int *) * (y + 1))))
		perrmsg("Cannot allocate gamemap array");
	map[y] = 0;
	if (!(map[0] = (int *)malloc(sizeof(int) * size)))
		perrmsg("Cannot allocate gamemap array");
	while (i < y)
	{
		map[i] = map[0] + (x * i);
		++i;
	}
	return (map);
}

void		read_info(char **arr, t_gamemap *gamemap)
{
	int		i;
	int		j;

	i = -1;
	while (++i < 4)
	{
		j = -1;
		while (arr[i][++j])
			if (!ft_isdigit(arr[i][j]))
				errmsg("Map must have only digits");
	}
	gamemap->map_width = ft_atoi(arr[0]);
	gamemap->map_height = ft_atoi(arr[1]);
	gamemap->player[0] = ft_atoi(arr[2]);
	gamemap->player[1] = ft_atoi(arr[3]);
}
