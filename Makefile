.PHONY: all clean fclean re

UNAME=$(shell uname)

CC=clang
CFLAGS=-Wall -Wextra -Werror -Ilibft/includes -Iincludes -o3
NAME=wolf3d
OBJDIR=objs
SRCDIR=srcs

MLXI=mlx_wrapper.c move_triggers.c

MAIN=main.c camera.c color.c math.c parse_map.c parse_map_help.c errors.c \
	 render.c putpixel.c textures.c floor_ceiling.c

SRC= $(MAIN) $(MLXI)

OBJ=$(SRC:.c=.o)
LIBFT=libft/libft.a

ifeq ($(UNAME), Linux)
MLXINCLUDES=-lm -lXext -lX11
MLX=libmlx/libmlx_Linux.a
endif

ifeq ($(UNAME), Darwin)
MLXINCLUDES=-framework OpenGL -framework AppKit -I/usr/local/include -L/usr/local/lib -lmlx
endif

all: $(OBJDIR) SUBMODULE $(NAME)

$(OBJDIR):
	@mkdir -p objs

SUBMODULE:
	@git submodule update --init
	@echo "Compiling Wolf"

ifeq ($(UNAME), Linux)
$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBFT) $(MLX)
	@$(CC) $(CFLAGS) $(MLXINCLUDES) -o $@ $^ -lpthread
	@echo ""
	@echo "Wolf compiled"

$(MLX):
	$(MAKE) --no-print-directory -C libmlx
	@echo "MLX compiled"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@printf "."
	@$(CC) $(CFLAGS) -Ilibmlx -c -o $@ $<

debug:
	$(CC) $(CFLAGS) $(MLXINCLUDES) $(addprefix $(SRCDIR)/, $(SRC)) libmlx/libmlx_Linux.a libft/libft.a -Ilibmlx -o $(NAME) -g -lpthread

endif

ifeq ($(UNAME), Darwin)
$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBFT)
	@$(CC) $(CFLAGS) $(MLXINCLUDES) -o $@ $^ -g
	@echo ""
	@echo "Wolf compiled"

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@printf "."
	@$(CC) $(CFLAGS) -c -o $@ $< -g

debug:
	$(CC) $(CFLAGS) $(MLXINCLUDES) $(addprefix $(SRCDIR)/, $(SRC))  libft/libft.a -o $(NAME) -g -lpthread
endif

$(LIBFT):
	@echo ""
	@$(MAKE) --no-print-directory -C libft

clean:
	@$(MAKE) --no-print-directory -C libft clean
	@$(MAKE) --no-print-directory -C libmlx clean
	@rm -rf $(OBJDIR)
	@echo "cleaned"

fclean:
	@$(MAKE) --no-print-directory -C libft fclean
	@$(MAKE) --no-print-directory -C libmlx clean
	@rm -rf $(OBJDIR)
	@rm -f $(NAME)
	@echo "fcleaned"

re: fclean all
