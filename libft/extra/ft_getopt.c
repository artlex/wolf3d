/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getopt.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/01 15:39:03 by bcherkas          #+#    #+#             */
/*   Updated: 2018/12/01 17:11:28 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	isopt(char *str)
{
	int		i;
	int		len;

	len = ft_strlen(str);
	if (len < 2)
		return (0);
	if (str[0] != '-')
		return (0);
	if (str[1] == '-' && len < 3)
		return (0);
	i = 1;
	while (str[++i])
		if (ft_isspace(str[i]))
			return (0);
	return (1);
}

static int	get_short_opts(char *str, t_opt *opts)
{
	int		i;
	int		j;

	j = 1;
	while (str[j])
	{
		i = -1;
		while (opts[++i].optname)
			if (!opts[i].longopt)
			{
				if (str[j] == opts[i].optname[0])
				{
					opts[i].sign = 1;
					break ;
				}
			}
		++j;
		if (!opts[i].optname)
			return (0);
	}
	return (1);
}

static int	check_opts(char *str, t_opt *opts)
{
	int		i;

	i = -1;
	if (str[1] == '-')
	{
		while (opts[++i].optname)
			if (opts[i].longopt)
				if (ft_strequ(str + 2, opts[i].optname))
				{
					opts[i].sign = 1;
					return (1);
				}
		return (0);
	}
	if (get_short_opts(str, opts))
		return (1);
	return (0);
}

int			ft_getopt(int ac, char **av, t_opt *opts)
{
	int		ret;
	int		i;

	i = 0;
	ret = 0;
	while (++i < ac)
	{
		if (isopt(av[i]))
		{
			ret = check_opts(av[i], opts);
			if (ret == 0)
				return (0);
		}
	}
	return (1);
}
