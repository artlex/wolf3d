/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   precision.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/18 18:58:11 by bcherkas          #+#    #+#             */
/*   Updated: 2018/02/21 14:29:57 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>

static void	precision(char **str, t_pf *format, va_list ap)
{
	ssize_t	i;

	format->prec = 0;
	if (**str != '.')
		return ;
	(*str)++;
	i = ft_atoi(*str) + 1;
	if (**str == '*')
	{
		(*str)++;
		i = va_arg(ap, int) + 1;
	}
	while (**str == '*' || (**str >= '0' && **str <= '9') ||
		**str == '-' || **str == '+')
		(*str)++;
	format->prec = i < 0 ? 0 : i;
}

void		width(char **str, t_pf *format, va_list ap)
{
	ssize_t	i;

	format->width = 0;
	i = ft_atoi(*str);
	while (**str >= '0' && **str <= '9')
		(*str)++;
	if (**str == '*')
	{
		(*str)++;
		i = va_arg(ap, int);
	}
	while (**str == '*' || (**str >= '0' && **str <= '9') ||
		**str == '-' || **str == '+')
		(*str)++;
	format->min = i < 0 ? 1 : format->min;
	format->width = i < 0 ? -i : i;
	precision(str, format, ap);
}
