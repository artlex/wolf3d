/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fprintf.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 20:14:03 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/29 20:50:30 by bcherkas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <stdarg.h>

int			ft_asprintf(char **str, const char *format, ...)
{
	va_list	ap;
	t_buf	*buflst;
	char	*strg;

	strg = (char *)format;
	buflst = NULL;
	va_start(ap, format);
	if (!(printf_core(strg, &buflst, ap)))
		return (saveexit(buflst, ap));
	va_end(ap);
	return (allocstring(buflst, str));
}

int			ft_dprintf(int fd, const char *format, ...)
{
	va_list	ap;
	t_buf	*buflst;
	char	*strg;

	strg = (char *)format;
	buflst = NULL;
	va_start(ap, format);
	if (!(printf_core(strg, &buflst, ap)))
		return (saveexit(buflst, ap));
	va_end(ap);
	return (printbuff(buflst, fd));
}

int			ft_snprintf(char *str, size_t size, const char *format, ...)
{
	va_list	ap;
	t_buf	*buflst;
	char	*strg;

	strg = (char *)format;
	buflst = NULL;
	va_start(ap, format);
	if (!(printf_core(strg, &buflst, ap)))
		return (saveexit(buflst, ap));
	va_end(ap);
	return (printtostring(buflst, str, size));
}

int			ft_printf(const char *format, ...)
{
	va_list	ap;
	t_buf	*buflst;
	char	*strg;

	strg = (char *)format;
	buflst = NULL;
	va_start(ap, format);
	if (!(printf_core(strg, &buflst, ap)))
		return (saveexit(buflst, ap));
	va_end(ap);
	return (printbuff(buflst, 1));
}
